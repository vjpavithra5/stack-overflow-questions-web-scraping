
import requests
from bs4 import BeautifulSoup as bs
import time
import pandas as pd


def clean_data(text, key=None):
    if key == 'votes':
        return text.replace('\nvotes','').replace('\n','')   
    return text.replace('\n','').strip()


def parse_data(response):
    data = bs(response, 'html.parser')
    questions = data.find_all(class_='question-summary')
    classes = ['question-hyperlink', 'vote', 'tags']
    keys = ['question', 'votes', 'tags']
    datas = []
    for el in questions:
        question = {}
        for i, c in enumerate(classes):
            content = el.find(class_=c).text
            key_name = keys[i]
            question[key_name] = clean_data(content, key=key_name)
        datas.append(question)    
    return datas


def extract_data(url):
    response = requests.get(url)
    if response.status_code == 200:
        return parse_data(response.content)
    else:
        return response.content


def scrape(tag='python', query_filter='Votes', page_limit=5, page_size=10):
    base_url = 'https://stackoverflow.com/questions/tagged/'
    datas = []
    for page_num in range(1, page_limit+1):
        url = f"{base_url}{tag}?tab={query_filter}&page={page_num}&pagesize={page_size}"
        datas += extract_data(url)
        time.sleep(3)
    return datas



data = scrape()

sf_df = pd.DataFrame(data)
sf_df.to_csv('stackoverflow.csv')



